# sass

## Instalación

* _Ruby_
```
apt-get install ruby-full
```
* sass
```
sudo su -c "gem install sass"
```

## Ejecución

```
sass <input.sass>:<output.css>
```

#### Automático
```
sass --watch <input.sass>:<output.css>
```
